% Example of parameters and function calls for 'offlineb1shimming' code
% This is an example of an 8-channel head coil

% Paths to B1+ and SAR data
B1_fullpath = [pwd '/offlineb1shimming/B1_HeadCoil.mat'];
SAR_fullpath = [pwd '/offlineb1shimming/SARMatrices_HeadCoil.mat'];

% These parameters are described in the header of B1Shim.m
CPphases = [0,-46,100,17,-58,-159,-16,-98];
algorithm = 1;
SED = 1.5;
PhaseOnly = 0;
lambda = 5e7; %5e7 will give you about equal weighting between uniformity and power
VoltageScaleFactor = [];
Constrained = 0;

%% ************************************************************************

% Read in Simulated B1+ maps
B1perVpeak_ch = ReadSimulatedB1Maps(B1_fullpath,VoltageScaleFactor);

% Reduce the B1+ maps to an nVox x nCoils matrix just including voxels
% within the defined ROI that have non-zero B1+.
[B1perVpeak_ch_roi,ROI_Mask] = CreateROI(B1perVpeak_ch);

% Perform B1+ shimming
B1Shim(B1perVpeak_ch, B1perVpeak_ch_roi, ROI_Mask, SAR_fullpath, algorithm, SED, CPphases, PhaseOnly, lambda, Constrained);