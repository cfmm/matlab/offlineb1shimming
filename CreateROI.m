function [B1perVpeak_ch_roi, ROI_Mask] = CreateROI(B1perVpeak_ch,Method,RotAngle,InferiorSliceExtent,SlabWidth)
%% *************************************************************************
% FUNCTION:    [B1perVpeak_ch_roi, ROI_Mask] = CreateROI(B1perVpeak_ch,Method,RotAngle,InferiorSliceExtent,SlabWidth)
% 
% DESCRIPTION: This function takes in B1+ maps (typically in radiological
%              convention) for each channel and allows the user to either
%              select an arbitrary ROI in the sagittal plane, or to define
%              the slab by the inferior slice extent, rotation angle, and
%              slab thickness. The ROI is then projected through all
%              sagittal slices and used to mask the B1+ maps to the ROI.
%              Only non-zero-B1+ voxels are included in the masked B1+ maps
%
% INPUTS:      B1perVpeak_ch - B1+ matrix for each channel
%               (nPts_dim1 x nPts_dim2 x nPts_dim3 x nCoils) [typically nT/V]
%              Method - Choose 'arb' or 'slab' to define the method of
%               determining an ROI. 'arb' will allow you to choose an
%               arbitrary ROI in the sagittal plane that will be projectd
%               through all sagittal slices. 'slab' will allow you to
%               specify the coordinates (RotAngle, InferiorSliceExtent,
%               SlabWidth) of a slab.
%              RotAngle - Angle of slab for method 'slab' [deg]
%              InferiorSliceExtent - inferior-most pixel number of the
%               slab. This can be determined by the y data-tip in the
%               sagittal image that is produced in method 'slab' [pixel number]
%              SlabWidth - width of slab in method 'slab' [pixels]
%
% OUTPUTS:     B1perVpeak_ch_roi - B1+ over the chosen ROI
%               user defined ROI (nVoxels x nCoils)
%              ROI_Mask - logical sagittal image of the ROI mask. This mask
%               is the same in all sagittal slices
%               (nPts_dim1 x nPts_dim2 x nPts_dim3)
%
% Created by:  Kyle M. Gilbert
%              June 2020 (KMG)
%
% Copyright:   Kyle M. Gilbert
% ************************************************************************
%% Initialize parameters
% If specific inputs are not provided later, the user will be promted to
% input them manually

narginchk(1, 5);

%% Create an ROI over which to B1+ shim

% Create a combined map that can be used for the ROI selection. It's just
% the sum of the absolute maps (i.e., not a correct B1+ map as phase is not
% considered). It is only used for selecting an ROI.
ImageVolume = sum(abs(B1perVpeak_ch),4);

% Number of coils
nCoils = size(B1perVpeak_ch,4);

% Query whether the user wants to define their own ROI using an arbitrary
% polygon over a sagittal slice, or prescribe an oblique slab using the
% inferior slice, rotation angle, and slab thickness. The sagittal ROI will
% be projected through all slices.
if nargin < 2
    Method = input(sprintf('\nWould you like an arbitrary FOV or prescribed slab (arb/[slab]):   '),'s');
end
if isempty(Method) || ~(strcmp(Method,'arb') || strcmp(Method,'slab'))
    Method = 'slab';
end

% Allow the user to adjust the ROI as many times as they need
DefineNewShimVolume = 'y';
while strcmp(DefineNewShimVolume,'y')
    
    if strcmp(Method,'arb')
        % If an arbitrary ROI is desired, the user can select an arbitrary ROI
        % over a sagittal image through a GUI. This ROI will be propagated
        % through all sagittal slices.
        
        % Prompt the user to select an arbitrary ROI
        SagImage = rot90(squeeze(ImageVolume(:,floor(size(ImageVolume,2)./2),:)));
        [~,Sag_ROI_Mask] = getroi(SagImage, 'Choose ROI', []);
        Sag_ROI_Mask = fliplr(Sag_ROI_Mask.'); % reorient to be consistent with 'slab-selection' code below
        
        % Mask B1+ to the chosen ROI and remove all background pixels for optimization
        B1perVpeak_ch_masked_vector = MaskToROI(B1perVpeak_ch,Sag_ROI_Mask);
        B1perVpeak_ch_roi = reshape(B1perVpeak_ch_masked_vector,[length(B1perVpeak_ch_masked_vector)./nCoils nCoils]);
        
    else % 'slab selection'
        % Define the coordinates of two lines in the sagittal plane. This will
        % define a slab. All values between these lines will be included in the B1+
        % optimization. This allows for oblique ROIs and the selection of a slab.
        
        % Plot the sagittal image with the default slab set to the entire volume
        CalcCroppingLine(0,size(ImageVolume,3),size(ImageVolume,3),ImageVolume);

        % Prompt the user to define the coordinates of the slab in the sagittal plane.
        if nargin < 3
            RotAngle = input(sprintf('\nDefine the rotation angle [deg]:   '));
        end
        if isempty(RotAngle)
            RotAngle = 0; % default if no user input
        end
        while RotAngle <= -90 || RotAngle >= 90
            disp('Invalid rotation angle: -90 < Rotation angle < 90')
            RotAngle = input('Define the rotation angle [deg]:   ');
        end
        
        if nargin < 4
            fprintf(sprintf('\nRefer to sagittal image to determine inferior matrix index: 1 (superior) < index < %i (inferior)\n',size(ImageVolume,3)));
            InferiorSliceExtent = input('Define the most inferior matrix index over which to shim [pixel]:   ');
        end
        if isempty(InferiorSliceExtent)
            InferiorSliceExtent = size(ImageVolume,3); % Default is entire FOV if no user input
        end
        while InferiorSliceExtent < 1 || InferiorSliceExtent > size(ImageVolume,3)
            fprintf(sprintf('Invalid entry: 1 < index < %i\n',size(ImageVolume,3)));
            InferiorSliceExtent = input('Define the most inferior matrix index over which to shim [pixel]:   ');
        end
        
        if nargin < 5
            SlabWidth = input(sprintf('\nDefine the width of the slab [pixels]:   '));
        end
        if isempty(SlabWidth)
            SlabWidth = size(ImageVolume,3); % Default is entire FOV if no user input
        end
        
        % Calculate and plot the default line over which to crop the ROI
        Sag_ROI_Mask = CalcCroppingLine(RotAngle,InferiorSliceExtent,SlabWidth,ImageVolume);
       
    end
    
    % Prompt to see if the user would like to change the ROI
    DefineNewShimVolume = input(sprintf('\nWould you like to adjust the volume [y/n]:   '),'s');
    
    % If the user accepts the ROI, mask the B1 maps to the ROI and
    % remove background pixels. Check to see whether there more than
    % one voxel left for B1 shimming. If not, ask the user to re-define the
    % ROI.
    if ~strcmp(DefineNewShimVolume,'y')
        
        % Mask B1+ to the chosen ROI and remove all background pixels for optimization
        B1perVpeak_ch_masked_vector = MaskToROI(B1perVpeak_ch,Sag_ROI_Mask);
        B1perVpeak_ch_roi = reshape(B1perVpeak_ch_masked_vector,[length(B1perVpeak_ch_masked_vector)./nCoils nCoils]);
        
        % Check to see of there is more than one voxel with a non-zero
        % B1. If not, require the user to choose another ROI
        if length(B1perVpeak_ch_masked_vector) <= 1
            fprintf(sprintf('\nWARNING: Require >1 voxel with non-zero B1 to shim\nChoose new ROI\n'))
            DefineNewShimVolume = 'y';
        end
    else
        close(gcf);
    end
    
end

% Create 3D mask
ROI_Mask = permute(repmat(Sag_ROI_Mask,1,1,size(B1perVpeak_ch,2)),[1 3 2]);

end

%% ************************************************************************
function [SagMask,SI_coords_Sup] = CalcCroppingLine(RotAngle,InferiorSliceExtent,SlabWidth,B1map)
% Define the coordinates of two lines in the sagittal plane. All values
% between these lines will be included in the B1+ optimization. This allows 
% for oblique adjustment volumes and he optimization over a slab. The slab
% width is defined in pixels.

% Define the coordinates of each line in the sagittal plane.
LineSlope = tan(deg2rad(RotAngle)); % slope of each line
AP_coords = 1:size(B1map,1); % x coordinates (anterior-posterior direction) of each line

% Inferior slice line definition
InferiorSliceCoord = -LineSlope .* size(AP_coords,2) + InferiorSliceExtent; % defines y-intercept of inferior line in image
SI_coords_Inf = LineSlope .* AP_coords + InferiorSliceCoord; % coordinates of the inferior line for display

% Superior slice line definition
SuperiorSliceCoord = InferiorSliceCoord - SlabWidth + 1; % defines y-intercept of inferior line in image
SI_coords_Sup = LineSlope .* AP_coords + SuperiorSliceCoord; % coordinates of the inferior line for display

% Adjust the coordinates, if necessary, so the cropping lines do not extend
% beyond the image
SI_coords_Inf(SI_coords_Inf < 1) = 1;
SI_coords_Sup(SI_coords_Sup < 1) = 1;

SI_coords_Inf(SI_coords_Inf > size(B1map,3)) = size(B1map,3);
SI_coords_Sup(SI_coords_Sup > size(B1map,3)) = size(B1map,3);

% Create an logical ROI mask in the sagittal plane. All voxels between the 
% bounding lines will be set to 1.
SagMask = zeros(size(B1map,1),size(B1map,3));
for AP_idx = 1:size(B1map,1)
    SagMask(AP_idx,((size(B1map,3) - floor(SI_coords_Inf(1,AP_idx))) + 1):(size(B1map,3) - floor(SI_coords_Sup(1,AP_idx)) + 1)) = 1;
end

% Plot the sagittal image with the desired lines
figure(gcf);
imagesc(rot90(squeeze(B1map(:,floor(size(B1map,2)./2),:)))); % sagittal image
axis image off; grid off;
caxis([0 prctile(B1map(:),99)])
title('Use for determining ROI','FontSize',18)
colormap gray
drawnow
hold on
line(AP_coords,SI_coords_Inf,'Color','yellow','LineWidth',3); % inferior line
drawnow
hold on
line(AP_coords,SI_coords_Sup,'Color','yellow','LineWidth',3); % superior line
drawnow

end

%% ************************************************************************
function B1perVpeak_ch_roi = MaskToROI(B1perVpeak_ch,SagMask)
% Mask B1+ to the chosen ROI and remove all background pixels for optimization
% Apply the mask through all sagittal slices

% Loop through all sagittal slices and apply the ROI mask
B1perVpeak_ch_masked = zeros(size(B1perVpeak_ch));
for sag_idx = 1:size(B1perVpeak_ch,2)
    B1perVpeak_ch_masked(:,sag_idx,:,:) = B1perVpeak_ch(:,sag_idx,:,:) .* permute(SagMask,[1 3 2]);
end

% It is possible, although rare, that the B1+ map of a single channel has a
% voxel or voxels that are zero in a position where the other channels do
% not. The following determines the number of non-zero voxels in each
% channel's B1+ map. It will then mask all channels by the mask of the
% channel with the fewest non-zero voxels. This ensures each channel has
% the same number of non-zero voxels and the matrix can be properly
% reshaped later.
mask_ch = all(abs(B1perVpeak_ch_masked) > 0, 4);
mask = repmat(mask_ch,[1 1 1 size(B1perVpeak_ch,4)]);

% Remove all voxels set to zero (i.e., both outside the ROI and any 
% background voxels within the ROI (i.e, voxels that have a B1+ = 0) 
B1perVpeak_ch_roi = B1perVpeak_ch(mask);

end
