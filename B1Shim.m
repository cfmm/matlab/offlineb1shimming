function [ShimmedSpec,CPSpec,TxScaleFactors_Opt] = B1Shim(B1perVpeak_ch, B1perVpeak_ch_roi, ROI_Mask, SAR_fullpath, algorithm, SED, CPphases, PhaseOnly, lambda, Constrained)
%% *************************************************************************
% FUNCTION:    B1Shim(B1perVpeak_ch, B1perVpeak_ch_roi, SAR_fullpath, algorithm, SED, CPphases, PhaseOnly, lambda, Constrained)
% 
% DESCRIPTION: This function will determine TxScaleFactors to optimize the 
%              B1+ shim. Simulated B1+ and SAR maps are read in. A 
%              non-linear optimization is used to minimize the cost 
%              function while constraining the maximum 10-g-averaged 
%              SAR to SED-fold higher or lower than the SAR of the CP
%              (TrueForm) mode. The available cost functions, algorithms, 
%              are listed below. The B1 shim optimization can also be run
%              unconstrained by setting the 'Constrained' parameter to 0.
%
% INPUTS:      B1perVpeak_ch - full 3D B1+ map of each channel
%               (nPts_x x nPts_y x nPts_z x nCoils) [nT/V]
%              B1perVpeak_ch_roi - B1+ of each channel over the ROI over
%               which to shim (nVoxels x nCoils) [nT/V]
%              ROI_Mask - logical mask of the ROI as derived from 
%               CreateROI.m
%              B1_fullpath - .mat file for the 3D B1+ maps of each channel
%               (nPts_x x nPts_y x nPts_z x nCoils) [nT/V]
%              SAR_fullpath - .mat file for the local SAR matrices
%                (nCoils x nCoils x nVoxels)
%              algorithm - 1: minimizes the coefficient of variation
%                          2: minimizes l2-norm of B1 field and regularizes
%                             on the mean B1 squared (i.e., power) with the
%                             Tikhonov-regularization parameter, lambda
%                          3: maximizes the minimum B1
%                          4: minimizes the min-max difference
%                          5: minimizes l4-norm over the mean
%              SED - the factor to which the local SAR of the optimized
%               shim solution can be higher than the SAR of the CP mode. If
%               this is set too low, the optimization may not be able to
%               find a solution that satisifes this constraint.
%              CPphases - vector of phases for the CP mode [deg] (1 x
%               nCoils). If empty, this will default to zero phase.

%              PhaseOnly - flag if you want to optimize only on the phases.
%               If empty, this will default to 0.
%              lambda - Tikhonov-regularization parameter for algorithm #2.
%               If empty, this will default to 0.
%              Constrained - flag as to whether you want the optimization
%               constrained by SAR or not. The unconstrained optimization
%               is faster. If empty, this will default to zero (unconstrained)
% 
% OUTPUTS:     Prints to screen the CP mode and optimized performance specs
%              of the solutions. It also plots the CP mode B1+ map and the 
%              optimized B1+ map.
% 
% Created by:  Kyle M. Gilbert
%              April 2020 (KMG)
% 
% Copyright:   Kyle M. Gilbert
% ************************************************************************

%% Initialize parameters

% Check number of input parameters
narginchk(4, 10);

% Number of coils
nCoils = size(B1perVpeak_ch,4);

% If inputs don't exist, set to default values
if nargin < 10
   Constrained = false;
   if nargin < 9
      lambda = 0;
      if nargin < 8
         PhaseOnly = false;
         if nargin < 7
            CPphases = zeros(1,nCoils);
         end
      end
   end
end
if isempty(PhaseOnly)
    PhaseOnly = false;
end
if isempty(lambda)
    lambda = 0;
end
if isempty(CPphases)
    CPphases = zeros(1,nCoils);
end
if isempty(Constrained)
    Constrained = false;
end
if isempty(SED)
    SED = 1e6; % i.e., unconstrained
end
if ~(SED > 0)
    error('SED must be positive');
end

%% Calculate initial CP-mode combined B1+ maps and SAR values, which are required for a constrained optimization

% Normalize the TxScaleFactors (all weights of one), as this is the
% convention on the Siemens scanner.
TxScaleFactors_CP = ones(1,nCoils) .* exp(1i*deg2rad(CPphases)) ./ norm(ones(1,nCoils));

% Calculate the combined B1+ map in CP mode
CombinedB1Map_CP = CalcCombinedB1Map(TxScaleFactors_CP,B1perVpeak_ch);

% Determine the maximum SAR for CP mode
% SAR = U'AU, where U is the excitation vector and A is the local SAR
% matrices (nCoils x nCoils x nVoxels)
if (Constrained)
    load(SAR_fullpath, 'SARMatrices');
    LocalSAR_CP = real(sum(bsxfun(@times, conj(TxScaleFactors_CP(:)), sum(bsxfun(@times,SARMatrices, TxScaleFactors_CP(:).'),2)), 1)); % SAR for each 10-g-average volume
    maxSARLimit = max(LocalSAR_CP(:)) .* SED; % Maximum local SAR for given excitation [W/kg]
end

%% Set up and perform the optimization

% Define the coeffs as the real and imaginary components of the
% TxScaleFactors. The coeffs will be optimized in the minimization
% algorithm.
% Define the coeffs of the CP mode as the starting point for the
% optimization.
coeffs_CP(1:nCoils) = real(TxScaleFactors_CP);
coeffs_CP(nCoils+1:2*nCoils) = imag(TxScaleFactors_CP);

% Lower and upper bound for opimtization on real and imaginary components
% of the TxScaleFactors. This defines a unit circle.
lb = -1.*ones(1,2.*nCoils);
ub = ones(1,2.*nCoils);

% *************************************************************************
% This is the optimization routine.
% Optimize the TxScaleFactors to minimize the cost function, based on the
% algorithm, in a constrained or unconstrained manner.
if (Constrained) % Optimization constrained by SAR
    options = optimoptions(@fmincon,'PlotFcn','optimplotfval','MaxFunctionEvaluations',500*nCoils*2);
    coeffs_Opt = fmincon(@(coeffs) CalcCostFunction(coeffs,B1perVpeak_ch_roi,algorithm,PhaseOnly,lambda), coeffs_CP,[],[],[],[],lb,ub,@(coeffs)constrfct(coeffs,SARMatrices,maxSARLimit),options);
else % Unconstrained optimization (faster)
    options = optimoptions(@fminunc,'PlotFcn','optimplotfval','MaxFunctionEvaluations',500*nCoils*2);
    coeffs_Opt = fminunc(@(coeffs) CalcCostFunction(coeffs,B1perVpeak_ch_roi,algorithm,PhaseOnly,lambda), coeffs_CP, options);
end
% *************************************************************************

% Determine the optimal TxScaleFactors from the coeffs and calculate the 
% shimmed combined B1 map.
TxScaleFactors_Opt = CalcTxScaleFactors(coeffs_Opt,PhaseOnly);
CombinedB1Map_Opt_roi = CalcCombinedB1Map(TxScaleFactors_Opt,B1perVpeak_ch_roi);
CombinedB1Map_Opt = CalcCombinedB1Map(TxScaleFactors_Opt,B1perVpeak_ch);

%% CP performance metrics

% Calculate the combined B1 map over the reduced FOV
CombinedB1Map_CP_roi = CalcCombinedB1Map(TxScaleFactors_CP,B1perVpeak_ch_roi);

% Performance metrics
CPSpec.MeanB1 = mean(CombinedB1Map_CP_roi(logical(CombinedB1Map_CP_roi)));
CPSpec.Prctile_80_B1 = prctile(CombinedB1Map_CP_roi(logical(CombinedB1Map_CP_roi)),80);
CPSpec.StdB1 = std(CombinedB1Map_CP_roi(logical(CombinedB1Map_CP_roi)));
CPSpec.CoeffVar = CPSpec.StdB1 ./ CPSpec.MeanB1;

if (Constrained)
    LocalSAR_CP = real(sum(bsxfun(@times, conj(TxScaleFactors_CP(:)), sum(bsxfun(@times,SARMatrices, TxScaleFactors_CP(:).'),2)), 1));
    CPSpec.MaxLocalSAR = max(LocalSAR_CP(:)); % Maximum local SAR for CP excitation [W/kg]
    CPSpec.MeanB1perSqrtSAR = 0.001 .* (CPSpec.MeanB1 ./ sqrt(CPSpec.MaxLocalSAR)); % [uT/sqrt(W/kg)]
    CPSpec.Prctile_80_B1perSqrtSAR = 0.001 .* (CPSpec.Prctile_80_B1 ./ sqrt(CPSpec.MaxLocalSAR)); % [uT/sqrt(W/kg)]
end

% Plot the CP mode B1+ map
cmax = prctile(CombinedB1Map_CP(:),99.5);
PlotB1Maps(CombinedB1Map_CP, cmax, sprintf('B1+ efficiency: CP mode [Cov: %0.2f percent]',100.*CPSpec.CoeffVar),ROI_Mask);

%% Shimmed performance metrics

% Performance metrics
ShimmedSpec.MeanB1 = mean(CombinedB1Map_Opt_roi(logical(CombinedB1Map_Opt_roi)));
ShimmedSpec.Prctile_80_B1 = prctile(CombinedB1Map_Opt_roi(logical(CombinedB1Map_Opt_roi)),80);
ShimmedSpec.StdB1 = std(CombinedB1Map_Opt_roi(logical(CombinedB1Map_Opt_roi)));
ShimmedSpec.CoeffVar = ShimmedSpec.StdB1 ./ ShimmedSpec.MeanB1;

if (Constrained)
    LocalSAR_Opt = real(sum(bsxfun(@times, conj(TxScaleFactors_Opt(:)), sum(bsxfun(@times,SARMatrices, TxScaleFactors_Opt(:).'),2)), 1));
    ShimmedSpec.MaxLocalSAR = max(LocalSAR_Opt(:)); % Maximum local SAR for given excitation [W/kg]
    ShimmedSpec.MeanB1perSqrtSAR = 0.001 .* (ShimmedSpec.MeanB1 ./ sqrt(ShimmedSpec.MaxLocalSAR)); % [uT/sqrt(W/kg)]
    ShimmedSpec.Prctile_80_B1perSqrtSAR = 0.001 .* (ShimmedSpec.Prctile_80_B1 ./ sqrt(ShimmedSpec.MaxLocalSAR)); % [uT/sqrt(W/kg)]
end

% Plot the shimmed B1+
PlotB1Maps(CombinedB1Map_Opt, cmax, sprintf('B1+ efficiency: Shimmed [%0.2f percent]',100.*ShimmedSpec.CoeffVar),ROI_Mask)

%% Print to screen the performance metrics and TxScaleFactors

fprintf(sprintf('\nOriginal CoV [percent]: %0.2f\n',100.*CPSpec.CoeffVar));
fprintf(sprintf('Original mean B1 [nT/V]: %0.2f\n',CPSpec.MeanB1));
fprintf(sprintf('Original 80th percentile B1 [nT/V]: %0.2f\n',CPSpec.Prctile_80_B1));
if (Constrained)
    fprintf(sprintf('Original max local SAR [(W/kg)/V]: %0.5f\n',CPSpec.MaxLocalSAR));
    fprintf(sprintf('Original mean B1/sqrt(max local SAR) [uT/sqrt(W/kg)]: %0.3f\n',CPSpec.MeanB1perSqrtSAR));
    fprintf(sprintf('Original 80th percentile B1/sqrt(max local SAR) [uT/sqrt(W/kg)]: %0.3f\n',CPSpec.Prctile_80_B1perSqrtSAR));
end

fprintf(sprintf('\nShimmed CoV [percent]: %0.2f  (%0.1f percent)\n',100.*ShimmedSpec.CoeffVar, 100 .* (1 - (ShimmedSpec.CoeffVar./CPSpec.CoeffVar)) ));
fprintf(sprintf('Shimmed mean B1 [nT/V]: %0.2f  (%0.1f percent)\n',ShimmedSpec.MeanB1, -100.* (1- (ShimmedSpec.MeanB1 ./ CPSpec.MeanB1))  ));
fprintf(sprintf('Shimmed 80th percentile B1 [nT/V]: %0.2f  (%0.1f percent)\n',ShimmedSpec.Prctile_80_B1, -100.* (1- (ShimmedSpec.Prctile_80_B1 ./ CPSpec.Prctile_80_B1)) ));
if (Constrained)
    fprintf(sprintf('Shimmed max local SAR: %0.5f  (%0.1f percent)\n',ShimmedSpec.MaxLocalSAR, -100.* (1- (ShimmedSpec.MaxLocalSAR ./ CPSpec.MaxLocalSAR))  ));
    fprintf(sprintf('Shimmed mean B1/sqrt(max local SAR) [uT/sqrt(W/kg)]: %0.3f  (%0.1f percent)\n',ShimmedSpec.MeanB1perSqrtSAR, -100.* (1- (ShimmedSpec.MeanB1perSqrtSAR ./ CPSpec.MeanB1perSqrtSAR))  ));
    fprintf(sprintf('Shimmed 80th percentile B1/sqrt(max local SAR) [uT/sqrt(W/kg)]: %0.3f  (%0.1f percent)\n\n',ShimmedSpec.Prctile_80_B1perSqrtSAR, -100.* (1- (ShimmedSpec.Prctile_80_B1perSqrtSAR ./ CPSpec.Prctile_80_B1perSqrtSAR))  ));
end


% Scale TxScaleFactors to have the same reference voltage as the TrueForm
% mode
CPratio = CPSpec.Prctile_80_B1 ./ ShimmedSpec.Prctile_80_B1;
TxScaleFactors_Opt = TxScaleFactors_Opt .* CPratio;

% Scale TxScaleFactors to have the reference voltage set to the 90th
% percentile
PrctileRatio = ShimmedSpec.Prctile_80_B1 ./ prctile(CombinedB1Map_Opt_roi(logical(CombinedB1Map_Opt_roi)),90);
TxScaleFactors_Opt = TxScaleFactors_Opt .* PrctileRatio;

% Make phase relative to the first channel
Phase = rad2deg(angle(TxScaleFactors_Opt) - angle(TxScaleFactors_Opt(1)));

% Print to screen the optimized TxScaleFactors
fprintf(sprintf('\nPhase: \n'));
fprintf('%0.1f ', Phase);
fprintf(sprintf('\nMag: \n'));
fprintf('%0.3f ',abs(TxScaleFactors_Opt));
fprintf(newline);

end

%% ************************************************************************
function cost = CalcCostFunction(coeffs,B1perVpeak_ch,algorithm,PhaseOnly,lambda)
% This is the cost function for the non-linear minimization. It calculates
% the cost function of the prescribed slices of the B1 map.

% Calculate the TxScaleFactors from the coeffs
TxScaleFactors = CalcTxScaleFactors(coeffs,PhaseOnly);

% Compute the B1+ map for the given TxScaleFactors
CombinedB1Map = CalcCombinedB1Map(TxScaleFactors,B1perVpeak_ch);

% Compute common performance metrics
MeanB1 = mean(CombinedB1Map(logical(CombinedB1Map)));
StdB1 = std(CombinedB1Map(logical(CombinedB1Map)));
absB1 = abs(CombinedB1Map(logical(CombinedB1Map)));

% COST FUNCTIONS
if algorithm == 1
    % Coeffcient of variation (std/mean)
    cost = StdB1 ./ MeanB1;
    
elseif algorithm == 2
    % Uniformity (l2-norm) with regularization on power (i.e., the inverse 
    % of the efficiency squared)
    cost = norm( absB1 - mean(absB1)).^2 + lambda .* (1 ./ MeanB1.^2);
    
elseif algorithm == 3
    % Maximize the mininmum B1
    cost = 1 ./ min(CombinedB1Map(logical(CombinedB1Map)));
    
elseif algorithm == 4
    % Min-max difference - kind of
    cost = (prctile(absB1,97) - prctile(absB1,3)) ./ MeanB1;
    
elseif algorithm == 5
    % Uniformity (l4-norm) -- kind of
    cost = ( norm(absB1 - mean(absB1)) ./ MeanB1 ).^4;
    
end

end

%% ************************************************************************
function CombinedB1Map = CalcCombinedB1Map(TxScaleFactors,B1perVpeak_ch)
% Calculate the combined B1 map with the given TxScaleFactors.

ndim = ndims(B1perVpeak_ch);
if ndim == 4
    p = [2 3 4 1];
elseif ndim == 3
    p = [2 3 1];
else
    p = [2 1];
end

CombinedB1Map = abs(sum(bsxfun(@times, B1perVpeak_ch, permute(TxScaleFactors(:), p)), ndim));

end

%% ************************************************************************
function TxScaleFactors = CalcTxScaleFactors(coeffs,PhaseOnly)
% Create normalized complex scale factors from the 'coeffs' variable, where
% the first half values are the real components of the TxScaleFactors and
% the remaining values are the imaginery components. Normalize by
% the norm of the weights so as not to influence the optimization by having
% a higher or lower weights. This is also the Siemens convention. If a
% phase-only shim is desired, only use the phase of the TxScaleFactors.
TxScaleFactors = complex(coeffs(1:end/2), coeffs(end/2+1:end));

% If you only want a phase-only shim
if nargin >1 && (PhaseOnly)
    phase = angle(TxScaleFactors); % dynamic
    TxScaleFactors = exp(1i*phase); % overwrite TxScaleFactors
end

% Normalize the TxScaleFactors
TxScaleFactors = TxScaleFactors ./ norm(TxScaleFactors);

end

%% ************************************************************************
function [c,ceq] = constrfct(coeffs,SARMatrices,maxSAR)
% This is the non-linear constraint on the local SAR. It requires
% the solution to have a maximum SAR less than SED-fold that of the CP mode
% (maxSAR = maxSAR_CPmode * SED).

% Calculate the TxScaleFactors from the coeffs
TxScaleFactors = CalcTxScaleFactors(coeffs);

% Calculate the local SAR for each voxel for the TxScaleFactors
LocalSAR = real(sum(bsxfun(@times, conj(TxScaleFactors(:)), sum(bsxfun(@times,SARMatrices, TxScaleFactors(:).'),2)), 1));

% Find the 10-g-averaged voluem that has the maximum local SAR
MaxLocalSAR = max(LocalSAR(:)); % Maximum local SAR for given excitation [W/kg]

% The constraint value. This should always be < 0 to satify the constraint
c = MaxLocalSAR - maxSAR;
ceq = [];

end

%% ************************************************************************
function PlotB1Maps(B1map,cmax,PlotTitle,ROI_Mask)
% Plots B1+ maps [nT/V], with maximum colorbar level of cmax and given plot
% title. The inferior and superior slice extents help define the axial
% slices to plot. This is optimized for B1+ shimming over the head, where
% for a full-brain shim, the inferior slice should be in the cerebellum.

% Montage of axial slices
figure;
montage(B1map,'Size',[ceil(sqrt(size(B1map,3))) ceil(sqrt(size(B1map,3)))]);
colormap parula
caxis([0 cmax])
h = colorbar(gca,'FontSize',16);
ylabel(h,'B1+ [nT/V]','FontSize',18,'LineWidth',2);
title(PlotTitle,'FontSize',18)
drawnow

% Determine the inferior and superior extent of the ROI
SagMask = squeeze(ROI_Mask(:,1,:));
[~,SI_VoxelPosition] = find(SagMask == 1);
InferiorROIExtent = size(SagMask,2) - min(SI_VoxelPosition(:)) + 1; % most inferior extent of ROI
SuperiorROIExtent = size(SagMask,2) - max(SI_VoxelPosition(:)) + 1; % most superior extent of ROI

% Montage of three planes -- axial, sagittal, coronal
% Plot two axial slices: (1) through the central plane of the adjustment
% volume, and (2) midway between the previous plane and the most
% inferior slice of the adjustment volume
CentralAxSlice = size(B1map,3) - floor((InferiorROIExtent + SuperiorROIExtent) ./ 2); % axial slice #1
InferiorAxSlice = floor(( (size(B1map,3) - InferiorROIExtent) + CentralAxSlice) ./ 2); % axial slice #2

Im1 = B1map(:,:,InferiorAxSlice);
Im2 = B1map(:,:,CentralAxSlice);
Im3 = rot90(squeeze(B1map(:,floor(size(B1map,2)./2),:)));
Im4 = rot90(squeeze(B1map(floor(size(B1map,1)./2),:,:)));

figure;
montage({Im1,Im2,Im3,Im4},'Size',[1 4],'ThumbnailSize',[200 200]);
caxis([0 cmax]);
colormap parula;
h2 = colorbar(gca,'FontSize',16);
ylabel(h2,'B_1^+ efficiency [nT/V]','FontSize',18,'LineWidth',2);
title(PlotTitle,'FontSize',18);
drawnow 

end