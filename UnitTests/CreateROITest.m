% Unit tests for CreateROI.m
% Kyle M. Gilbert
% June 2020

% Load sample B1+ data required as input to GetROI.m
% Created with:
% B1perVpeak_ch = ReadSimulatedB1Maps([pwd '/offlineb1shimming/B1_HeadCoil.mat']);
UnitTestDirectory = [fileparts(mfilename('fullpath')) filesep];
importSampleData(UnitTestDirectory,'offlineB1shimmingtest_data.zip','offlineB1shimmingtest_data','19263325');

load([UnitTestDirectory 'offlineB1shimmingtest_data/CreateROI_TestInputData.mat']);

%% Test 1: 'slab' ROI

Method = 'slab';
RotAngle = 12;
InferiorSliceExtent = 70;
SlabWidth = 60;

[B1perVpeak_ch_roi,~] = CreateROI(B1perVpeak_ch,Method,RotAngle,InferiorSliceExtent,SlabWidth);
close all;

assert(size(B1perVpeak_ch_roi,1) - 178525 == 0);

%% Test 2: 'arb' ROI

Method = 'arb';
RotAngle = [];
InferiorSliceExtent = [];
SlabWidth = [];

[B1perVpeak_ch_roi,~] = CreateROI(B1perVpeak_ch,Method,RotAngle,InferiorSliceExtent,SlabWidth);
close all;

assert(size(B1perVpeak_ch_roi,1) > 0);