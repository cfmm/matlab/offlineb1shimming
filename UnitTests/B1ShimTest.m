% Unit tests for B1Shim.m
% Kyle M. Gilbert
% June 2020

% Load sample data required as input to B1Shim.m (B1perVpeak_ch,
% B1perVpeak_ch_roi, ROI_Mask)
UnitTestDirectory = [fileparts(mfilename('fullpath')) filesep];
importSampleData(UnitTestDirectory,'offlineB1shimmingtest_data.zip','offlineB1shimmingtest_data','19263325');

% SAR path
SAR_fullpath = which('SARMatrices_HeadCoil.mat');

% ROI data
% Created with:
% B1perVpeak_ch = ReadSimulatedB1Maps([datadir '/offlineb1shimming/B1_HeadCoil.mat']);
% [B1perVpeak_ch_roi, ROI_Mask] = GetROI(B1perVpeak_ch,'slab',12,70,60)
load([UnitTestDirectory 'offlineB1shimmingtest_data/B1Shim_TestInputData.mat']);

%% Test 1: Alogrithm 1 (min CoV, unconstrained)

% These parameters are described in the header of B1Shim.m
CPphases = [0,-46,100,17,-58,-159,-16,-98];
algorithm = 1;
SED = [];
PhaseOnly = 0;
lambda = []; %5e7 will give you about equal weighting between uniformity and power
Constrained = 0;

[ShimmedSpec,~] = B1Shim(B1perVpeak_ch, B1perVpeak_ch_roi, ROI_Mask, SAR_fullpath, algorithm, SED, CPphases, PhaseOnly, lambda, Constrained);
close all;

assert(abs(ShimmedSpec.CoeffVar - 0.2654) < 0.0001);

%% Test 2: Phase-only optimization - Alogrithm 1

% These parameters are described in the header of B1Shim.m
CPphases = [0,-46,100,17,-58,-159,-16,-98];
algorithm = 1;
SED = [];
PhaseOnly = 1;
lambda = []; %5e7 will give you about equal weighting between uniformity and power
Constrained = 0;

[ShimmedSpec,~] = B1Shim(B1perVpeak_ch, B1perVpeak_ch_roi, ROI_Mask, SAR_fullpath, algorithm, SED, CPphases, PhaseOnly, lambda, Constrained);
close all;

assert(abs(ShimmedSpec.CoeffVar - 0.2927) < 0.0001);

%% Test 3: Algorithm 2 (l2-norm, regularize power)

% These parameters are described in the header of B1Shim.m
CPphases = [0,-46,100,17,-58,-159,-16,-98];
algorithm = 2;
SED = 1.5;
PhaseOnly = 0;
lambda = 5e7; %5e7 will give you about equal weighting between uniformity and power
Constrained = 0;

[ShimmedSpec,~] = B1Shim(B1perVpeak_ch, B1perVpeak_ch_roi, ROI_Mask, SAR_fullpath, algorithm, SED, CPphases, PhaseOnly, lambda, Constrained);
close all;

assert(abs(ShimmedSpec.CoeffVar - 0.3535) < 0.0001);

%% Test 4: Algorithm 3 (min B1)

% These parameters are described in the header of B1Shim.m
CPphases = [0,-46,100,17,-58,-159,-16,-98];
algorithm = 3;
SED = 1.5;
PhaseOnly = 0;
lambda = 5e7; %5e7 will give you about equal weighting between uniformity and power
Constrained = 0;

[ShimmedSpec,~] = B1Shim(B1perVpeak_ch, B1perVpeak_ch_roi, ROI_Mask, SAR_fullpath, algorithm, SED, CPphases, PhaseOnly, lambda, Constrained);
close all;

assert(abs(ShimmedSpec.CoeffVar - 0.3108) < 0.0001);

%% Test 5: Constrained optimization - Algorithm 1

% These parameters are described in the header of B1Shim.m
CPphases = [0,-46,100,17,-58,-159,-16,-98];
algorithm = 1;
SED = 1.5;
PhaseOnly = 0;
lambda = 5e7; %5e7 will give you about equal weighting between uniformity and power
Constrained = 1;

[ShimmedSpec,~] = B1Shim(B1perVpeak_ch, B1perVpeak_ch_roi, ROI_Mask, SAR_fullpath, algorithm, SED, CPphases, PhaseOnly, lambda, Constrained);
close all;

assert(abs(ShimmedSpec.CoeffVar - 0.2654) < 0.0001);


