Downloading/initializing code
-------------
This respository, `offlineb1shimming`, requires additional code in other public CFMM repositories. The following repositories should be cloned:

https://gitlab.com/cfmm/matlab/common

Functions and example data for offline B1+ shimming
-------------

The main function is `B1Shim.m`. An example of the input parameters and function calls are found in `OfflineShimmingParams_*.m`. Sample `.mat` files are included for SAR and B1+ fields; these `.mat` files use lfs tracking.

Unit Tests
-------------
Unit tests are provided with sample data for evaluating code modifications.

General Workflow
-------------
1.  Read in simulated B1 maps (`ReadSimulatedB1Maps.m`)
2.  Choose an ROI over which to B1+ shim (`CreateROI.m`)
3.  Perform B1+ shimming (`B1Shim.m`)

General Workflow
-------------
Unit tests are provided for `B1Shim.m` and `GetROI.m`. Sample data is provided for the input to these unit tests.